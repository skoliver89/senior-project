# Western Oregon University Senior Project 2018 #
## Group Project Ideas ##


### Conversational Interface for Web ###

A web interface that provides access to many web APIs all in one place that can act as a personal assistant. Using voice identification, the software can determine who is talking to it and get their relevant information.

### Dungeons and Dragons AI Dungeon Master ###

A Dungeon Master for Dungeons and Dragons that uses machine learning to setup scenarios and provide descriptions of locations, enemies, and objects in a D&D campaign.

### Personal API ###

A public interface for virtual and real-world interactions. Provides a generic public API with permission-controlled access to your personal information. Employment information, medical data (allergies), and contact information all in one public interface.

### Merged Conversation Engine with AI Moderation ###

A chat tool for live streamers that stream to multiple streaming services like Twitch, YouTube, and Mixer. All the chat messages from these services in one place complete with machine learning enhanced moderation tools.

### Water System Layout Software ###

A water system layout tool for civil engineers that combines data from ArcGIS and Google Maps and provides simulation of water flow though systems.