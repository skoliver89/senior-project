Retrospective

Safety check
	Done, everyone wrote their number 1-5 anonymously on a piece of paper. We got a good score.
	
What did you learn from the sprint?
	--We learned the Git workflow. 
	--Procastinating to the last minute is a bad idea. 
	--Merge conflicts aren't that scary. 
	--We need better communication about naming schemes for Controllers and so on within the code.
	--Not recognizing dependencies is a bad thing.
	--We need to spend more time on database construction beforehand, so we don't have to refactor later on and break everything.
	--We need better documentation/structure on how to be a contributer, e.g., how to name feature branches and how to structure comments. This'll come up in the next Milestone though.

What still puzzles you?
	Stephen: Merge conflicts
	Jake: Merge conflicts 
	Abby: How we're supposed to have this many long meetings a week.

What can the team do better during the next sprint? Make a specific action plan.
	--Start working on code way earlier, i.e., before Friday night.
	--Better communication when we aren't face to face would be good. --Let's also add our contribution guidelines to the front page on the Bitbucket, instead of hidden within the filesystem.
	
Are there any items that need to be brought up with someone outside the team?
	Nope