# Primary Needs/Features

1. Athletes upload data to cloud DB.
1. Athletes, parents, coaches, and admins must be able to log into the service. 
1. Athletes can view ONLY their data.
1. Parents can view ONLY their athletes' data.
1. Coaches can view all athlete data from their team.
1. Coaches can track equipment assigned to athletes.
1. Admins can add and remove users.
1. Admins can add and remove equipment.
1. Admins can view DB logs.