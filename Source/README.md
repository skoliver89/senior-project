# Western Oregon University Senior Project 2018 #
## Source Folder ##

### What is this folder for? ###
This folder should contain all relevant code related to the project. This includes:

* Visual Studio Project Files.
* SQL Setup Scripts.
* Any additional scripts required to setup the build environment.