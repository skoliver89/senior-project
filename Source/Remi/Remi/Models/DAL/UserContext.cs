namespace Remi.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class UserContext : DbContext
    {
        public UserContext()
            : base("name=UserContext")
        {
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Profile> Profiles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .Property(e => e.UserName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.CipheredPassword)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.PasswordSalt)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.Alias)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.VoicePref)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.HomeLoc)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.HomeCity)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.HomeState)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.CalendarID)
                .IsUnicode(false); 
        }

    }
}
