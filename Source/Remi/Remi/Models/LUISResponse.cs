﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
/***********************************************************************************************\
 * LUIS Response class
 * Written by Devon Smith
 * Western Oregon University Winter 2018
 * Additional Contribution by: <none>
 * 
 * Class generated using: http://json2csharp.com/
 * 
 * This class is for deserialization of responses sent by LUIS to the Remi application.
 * This can make the processing and extraction of the information inside the Json response from
 * LUIS simpler.
 * 
 \**********************************************************************************************/
namespace Remi.Models
{
    /// <summary>
    /// Class that contains the formal class to deserialize LUIS Json responses. The enables the
    /// quick parsing of the information in the Json returned by LUIS into a specific response for
    /// Remi.
    /// </summary>
    public class LUISResponse
    {
        /// <summary>
        /// Stores the top scoring intent returned from LUIS.
        /// Low enough scores should be ignored and passed as unintelligible.
        /// </summary>
        public class LUISTopScoringIntent
        {
            // The intent
            public string intent { get; set; }
            // The numeric score from 0 - 1
            public double score { get; set; }
        }

        /// <summary>
        /// An intent returned from LUIS including the intent's name and score.
        /// </summary>
        public class LUISIntent
        {
            // The intent
            public string intent { get; set; }
            // The numeric score from 0 - 1
            public double score { get; set; }
        }

        /// <summary>
        /// An entity returned from LUIS including the entity, type, startIndex, endIndex, and the
        /// score of that intent.
        /// </summary>
        public class LUISEntity
        {
            // The entity
            public string entity { get; set; }
            // The entity type
            public string type { get; set; }
            // The starting index of the entity.
            public int startIndex { get; set; }
            // The ending index of the entity.
            public int endIndex { get; set; }
            // The score of that entity from 0 - 1
            public double score { get; set; }
        }

        /// <summary>
        /// The root object for the LUIS Json response.
        /// </summary>
        public class LUISRootObject
        {
            // The string passed to LUIS.
            public string query { get; set; }
            // The top scoring intent.
            public LUISTopScoringIntent topScoringIntent { get; set; }
            // The list of intents from LUIS
            public List<LUISIntent> intents { get; set; }
            // The list of possible entities from LUIs.
            public List<LUISEntity> entities { get; set; }
        }
    }
}