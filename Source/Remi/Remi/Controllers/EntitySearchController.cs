﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Net;
using System.IO;

namespace Remi.Controllers
{
    public class EntitySearchController : Controller
    {
        static string host = "https://api.cognitive.microsoft.com";
        static string path = "/bing/v7.0/entities";

        // Defualt market can be specified here.
        static string currentMarket = "en-US";

        // Get the key from the web configuration.
        static string key = System.Web.Configuration.WebConfigurationManager.AppSettings["BingEntitySearch"];

        /// <summary>
        /// Search endpoint for Bing Entity Search using Bing v7.0 API.
        /// </summary>
        /// <param name="query">The query string from the user.</param>
        /// <returns></returns>
        public ContentResult EntitySearch(string query)
        {
            //get the URL built
            string uri = EntitySearchString(currentMarket, query);
            //Send a request to Bing
            WebRequest request = WebRequest.Create(uri);
            request.Headers.Add("Ocp-Apim-Subscription-Key", key);

            //Get the response from Bing Entity Search
            WebResponse response = request.GetResponse();
            //Start the data stream
            Stream stream = response.GetResponseStream();
            string str = new StreamReader(stream).ReadToEnd();

            return Content(str, "application/json");
        }

        /// <summary>
        /// Creates a string for the Bing Entity Search API. This is an entry point for testing.
        /// </summary>
        /// <param name="market">The market for the api</param>
        /// <param name="query">The test query passed for search/</param>
        /// <returns></returns>
        public string EntitySearchStringStart(string market, string query)
        {
            return EntitySearchString(market, query);
        }

        /// <summary>
        /// Creates the search string fhr the Bin entity search. Compliant with Bing 7.0 API.
        /// </summary>
        /// <param name="market">The target market for the search. </param>
        /// <param name="query">The query from the user. </param>
        /// <returns></returns>
        private string EntitySearchString(string market, string query)
        {
            // Return the URI for the Bing v7.0 search API.
            return host + path + "?mkt=" + market + "&q=" + System.Net.WebUtility.UrlEncode(query);
        }

    }
}