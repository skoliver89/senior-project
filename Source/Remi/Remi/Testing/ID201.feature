Feature: I want to tell Remi to use my home location for the weather and have
Remi access the stored data of my home location to get the weather.

Scenario: Remi is able to access and use my home location to get the weather.
Given Remi already has my home location stored
And Remi has access to my home location
When I ask Remi "What is the weather at home?"
Then Remi accesses my stored home location and retrieves the weather for that
location

Scenario: Remi is unable to access my home location.
Given Remi is unable to access my stored home location for any reason
When I ask Remi "What is the weather at home?"
Then Remi responds with either something like "I couldn't find that location"
or "I'm having issues with my Ajax logic"

Scenario: Weather Underground is having errors and not returning the weather
Given Remi is able to access my stored home location and sends the request
When I ask Remi "What is the weather at home?"
Then Remi responds with either something like "I couldn't find that location"
or "Weather Underground is not responding"
