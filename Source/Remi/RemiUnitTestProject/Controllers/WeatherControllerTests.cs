﻿using Remi.Controllers;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using System;

namespace Remi.Controllers.Tests
{
    [TestFixture()]
    public class WeatherControllerTests
    {
        //Common Variables in tests - Stephen
        private WeatherController c;
        private string inputSalemOR;
        private string expectedSalem;
        private string expectedOR;
        private string inputMultiplePipes;
        private string inputNull;

        //Setup the common test variables - Stephen
        [SetUp()]
        protected void SetUp()
        {
            c = new WeatherController();
            inputSalemOR = "Salem|OR";
            expectedSalem = "Salem";
            expectedOR = "OR";
            inputMultiplePipes = "Salem|OR|something|fishing|is|going|on";
            inputNull = null;
        }

        // Jake's tests #1
        [Test()]
        public void GetCity_ValidCityPipeStateStringReturningNotNullShould_Pass()
        {
            string actualResult = c.GetCity(inputSalemOR);
            Assert.IsNotNull(actualResult);
        }

        // Jake's tests #2
        [Test()]
        public void GetCity_ValidCityPipeStateStringReturningNotEmptyShould_Pass()
        {
            string actualResult = c.GetCity(inputSalemOR);
            Assert.IsNotEmpty(actualResult);
        }

        // Jake's tests #3
        [Test()]
        public void GetCity_ValidCityPipeStateStringShould_Pass()
        {
            string actualResult = c.GetCity(inputSalemOR);
            Assert.AreEqual(actualResult, expectedSalem);
        }

        // Jake's tests #4
        [Test()]
        public void GetCity_MixedNumbersBeforePipeStringShould_Pass()
        {
            string input = "m0nm04th|OR";
            string expectedResult = "m0nm04th";

            string actualResult = c.GetCity(input);
            Assert.AreEqual(actualResult, expectedResult);
        }

        // Jake's tests #5
        [Test()]
        public void GetCity_MixedSymbolsBeforePipeStateStringShould_Pass()
        {
            string input = "$alem!|OR";
            string expectedResult = "$alem!";

            string actualResult = c.GetCity(input);
            Assert.AreEqual(actualResult, expectedResult);
        }

        // Jake's tests #6
        [Test()]
        public void GetCity_CityFollowedByMultiplePipesInStringShould_Pass()
        {
            string actualResult = c.GetCity(inputMultiplePipes);
            Assert.AreEqual(actualResult, expectedSalem);
        }

        // Jake's tests #7
        [Test()]
        public void GetCity_CityWithMixedCapsStringReturningCityShould_Pass()
        {
            string input = "mOnMoUtH|OR";
            string expectedResult = "mOnMoUtH";

            string actualResult = c.GetCity(input);
            Assert.AreEqual(actualResult, expectedResult);
        }

        // Jake's tests #8
        [Test()]
        public void GetCity_NullInputThrows_NullReferenceException()
        {
            Assert.Throws<NullReferenceException>(() => c.GetCity(inputNull));
        }

        // Stephen's Test #1
        [Test()]
        public void GetState_NullInputThrows_NullReferenceException()
        {
            ActualValueDelegate<object> testDelegate = () => c.GetState(inputNull);
            Assert.That(testDelegate, Throws.TypeOf<NullReferenceException>());
        }

        //Stephen's Test #2
        [Test()]
        public void GetState_ValidStateEntry()
        {
            string actual = c.GetState(inputSalemOR);
            Assert.That(actual, Is.EqualTo(expectedOR));
        }

        //Stephen's Test #3
        [Test()]
        public void GetState_InvalidStateEntry()
        {
            string input = "Salem|0R";
            string actual = c.GetState(input);
            Assert.That(actual, Is.Not.EqualTo(expectedOR));
        }

        //Stephen's Test #4
        [Test()]
        public void GetState_StateFollowedByMultiplePipes()
        {
            string actual = c.GetState(inputMultiplePipes);
            Assert.That(actual, Is.EqualTo(expectedOR));
        }

        //Stephen Test #5
        [Test()]
        public void GetState_StateIsEmpty()
        {
            string input = "Salem|";
            Assert.That(c.GetState(input), Is.EqualTo(string.Empty));
        }

        //Stepen Test #6
        [Test()]
        public void GetState_NoPipes_IndexOutOfRangeExceptionThrown()
        {
            string input = "Salem, OR";
            ActualValueDelegate<object> testDelegate = () => c.GetState(input);
            Assert.That(testDelegate, Throws.TypeOf<IndexOutOfRangeException>());
        }

        //Stephen Test #7
        [Test()]
        public void GetState_LowercaseIsInvalid()
        {
            string input = "Salem|or";
            Assert.That(c.GetState(input), Is.Not.EqualTo(expectedOR));
        }

        //Stephen Test #8
        [Test()]
        public void GetState_WithSymbols_IsInvalid()
        {
            string input = "Salem|*OR*";
            Assert.That(c.GetState(input), Is.Not.EqualTo(expectedOR));
        }

        //Stephen Test #9
        [Test()]
        public void GetState_FloppedFormat_IsInvalid()
        {
            string input = "OR|Salem";
            Assert.That(c.GetState(input), Is.Not.EqualTo(expectedOR));
        }

        //Jake Test F201
        [Test()]
        public void GetUserId_NoInputForUserId_IndexOutOfRangeExceptionThrown()
        {
            string input = "OR|Salem";
            ActualValueDelegate<object> testDelegate = () => c.GetUserId(input);
            Assert.That(testDelegate, Throws.TypeOf<IndexOutOfRangeException>());
        }

        //Jake Test F201
        [Test()]
        public void GetUserId_UserIdNotEntered_returnsDefault0()
        {
            string input = "Salem|OR|";
            Assert.That(c.GetUserId(input), Is.EqualTo(0));
        }

        //Jake Test F201
        [Test()]
        public void GetUserId_UserIdIsAString_returnsDefault0()
        {
            string input = "Salem|OR|one";
            Assert.That(c.GetUserId(input), Is.EqualTo(0));
        }

        //Jake Test F201
        [Test()]
        public void GetUserId_ValidCityPipeStatePipeUserIdStringReturningNotNullShould_Pass()
        {
            int actualResult = c.GetUserId("Aloha|Hawaii|54");
            Assert.IsNotNull(actualResult);
        }
    }
}