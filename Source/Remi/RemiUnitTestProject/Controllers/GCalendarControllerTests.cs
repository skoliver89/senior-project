﻿using NUnit.Framework;
using Remi.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace Remi.Controllers.Tests
{
    [TestFixture()]
    public class GCalendarControllerTests
    {
        private GCalendarController controller;
        // Get date data for this current time.
        // Current date:
        private static DateTime today = DateTime.Today;
        // Calculate the first day of the week
        private static DateTime previousSunday = DateTime.Today
            .AddDays(-1 * ((7 + (today.DayOfWeek - DayOfWeek.Sunday)) % 7));
        // Calculate the next Sunday
        private static DateTime nextSunday = previousSunday.AddDays(7);
        // Calculate the first day of the month
        private static DateTime startOfMonth = new DateTime(today.Year, DateTime.Today.Month, 1);
        // Calculate the last day of the month.
        private static DateTime endOfMonth = startOfMonth.AddMonths(1).AddTicks(-1);
        // Calculate the first day of the year
        private static DateTime startOfYear = new DateTime(today.Year, 1, 1);
        // Calculate the last day of the year
        private static DateTime endOfYear = new DateTime(today.Year, 12, 31);

        // Strings build from the date time data objects.
        private static string todayString = XmlConvert.ToString( today,
                                      XmlDateTimeSerializationMode.Local);
        private static string previousSundayString = XmlConvert.ToString( previousSunday ,
                                      XmlDateTimeSerializationMode.Local);
        private static string previousSaturdayString = XmlConvert.ToString(previousSunday.AddDays(-1),
                                      XmlDateTimeSerializationMode.Local);
        private static string previousPreviousSundayString = XmlConvert.ToString(previousSunday
                                                        .AddDays(-7),
                                      XmlDateTimeSerializationMode.Local);
        private static string nextSundayString = XmlConvert.ToString( nextSunday,
                                      XmlDateTimeSerializationMode.Local);
        private static string nextSaturdayString = XmlConvert.ToString(nextSunday.AddDays(-1),
                                      XmlDateTimeSerializationMode.Local);
        private static string nextNextSundayString = XmlConvert.ToString(nextSunday.AddDays(7),
                                      XmlDateTimeSerializationMode.Local);
        private static string nextNextSaturdayString = XmlConvert.ToString(nextSunday.AddDays(-1).AddDays(7),
                                      XmlDateTimeSerializationMode.Local);
        private static string previousStartOfMonthString = XmlConvert.ToString(startOfMonth.AddMonths(-1),
                                      XmlDateTimeSerializationMode.Local);
        private static string previousEndOfMonthString = XmlConvert.ToString(startOfMonth.AddTicks(-1),
                                      XmlDateTimeSerializationMode.Local);
        private static string startOfMonthString = XmlConvert.ToString( startOfMonth,
                                      XmlDateTimeSerializationMode.Local);
        private static string endOfMonthString = XmlConvert.ToString( endOfMonth,
                                      XmlDateTimeSerializationMode.Local);
        private static string nextStartOfMonthString = XmlConvert.ToString(startOfMonth.AddMonths(1),
                                      XmlDateTimeSerializationMode.Local);
        private static string nextEndOfMonthString = XmlConvert.ToString(endOfMonth.AddMonths(1),
                                      XmlDateTimeSerializationMode.Local);
        private static string startOfYearString = XmlConvert.ToString( startOfYear,
                                      XmlDateTimeSerializationMode.Local);
        private static string endOfYearString = XmlConvert.ToString(endOfYear ,
                                      XmlDateTimeSerializationMode.Local);
        private static string startOfNextYearString = XmlConvert.ToString(startOfYear.AddYears(1),
                                      XmlDateTimeSerializationMode.Local);
        private static string endOfNextYearString = XmlConvert.ToString(endOfYear.AddYears(1),
                                      XmlDateTimeSerializationMode.Local);
        private static string startOfLastYearString = XmlConvert.ToString(startOfYear.AddYears(-1),
                                      XmlDateTimeSerializationMode.Local);
        private static string endOfLastYearString = XmlConvert.ToString(endOfYear.AddYears(-1),
                                      XmlDateTimeSerializationMode.Local);

        // test URL for Calendar ID
        private static string calendarID = "noone%40example.com";
        // test api key this key must match what is used in the main project.
        private static string apiKey = "ABcdEfGHIJk123LmnOPq4rstuvWXYzA-bCdefgh";
        private static string googleCalendarString = "https://www.googleapis.com/calendar/v3/calendars/" + calendarID + "/events?key=";


        // Create strings to test against
        /// <summary>
        /// Default return if there is no time boxing.
        /// </summary>
        private string defaultCalendar;
        /// <summary>
        /// Default return if nulls are provided for the type, calendarID, and Api Key.
        /// </summary>
        private string defaultNullCalendar = "https://www.googleapis.com/calendar/v3/calendars//events?key=";
        /// <summary>
        /// Week return strings
        /// </summary>
        private string thisWeek;
        private string lastWeek;
        private string nextWeek;
        /// <summary>
        /// Month return strings.
        /// </summary>
        private string thisMonth;
        private string lastMonth;
        private string nextMonth;
        /// <summary>
        /// Year return strings.
        /// </summary>
        private string thisYear;
        private string lastYear;
        private string nextYear;

        [SetUp()]
        protected void SetUp()
        {
            controller = new GCalendarController();
            
            /// <summary>
            /// Initialize default return if there is no time boxing.
            /// </summary>
            defaultCalendar = googleCalendarString + apiKey;

            /// <summary>
            /// Initialize week return strings
            /// </summary>
            thisWeek = googleCalendarString + apiKey + "&timeMin=" + previousSundayString + "&timeMax=" + nextSaturdayString;
            lastWeek = googleCalendarString + apiKey + "&timeMin=" + previousPreviousSundayString + "&timeMax=" + previousSaturdayString;
            nextWeek = googleCalendarString + apiKey + "&timeMin=" + nextSundayString + "&timeMax=" + nextNextSaturdayString;
            /// <summary>
            /// Initialize month return strings.
            /// </summary>
            thisMonth = googleCalendarString + apiKey + "&timeMin=" + startOfMonthString + "&timeMax=" + endOfMonthString;
            lastMonth = googleCalendarString + apiKey + "&timeMin=" + previousStartOfMonthString + "&timeMax=" + previousEndOfMonthString;
            nextMonth = googleCalendarString + apiKey + "&timeMin=" + nextStartOfMonthString + "&timeMax=" + nextEndOfMonthString;
            /// <summary>
            /// Initialize year return strings.
            /// </summary>
            thisYear = googleCalendarString + apiKey + "&timeMin=" + startOfYearString + "&timeMax=" + endOfYearString;
            lastYear = googleCalendarString + apiKey + "&timeMin=" + startOfLastYearString + "&timeMax=" + endOfLastYearString;
            nextYear = googleCalendarString + apiKey + "&timeMin=" + startOfNextYearString + "&timeMax=" + endOfNextYearString;
        }
        // Devon test 1
        // Default call to the calendar
        [Test()]
        public void CalendarURLTestShouldPass()
        {
            string url = controller.CalendarURL(0, null, calendarID, apiKey);
            Assert.AreEqual(defaultCalendar, url);
        }

        /*******************************************************\
        * Tests for weeks
        \*******************************************************/
        // Devon test 2
        [Test()]
        public void CalendarURLTestWeekNowShouldPass()
        {
            string url = controller.CalendarURL(0, "week", calendarID, apiKey);
            url = Regex.Replace(url, @"\+", "%2b");
            Console.Write(thisWeek + ":" + url);
            Assert.AreEqual(thisWeek, url);
        }
        // Devon test 3
        [Test()]
        public void CalendarURLTestWeekLastShouldPass()
        {
            string url = controller.CalendarURL(-1, "week", calendarID, apiKey);
            url = Regex.Replace(url, @"\+", "%2b");
            Assert.AreEqual(lastWeek, url);
        }
        // Devon test 4
        [Test()]
        public void CalendarURLTestWeekNextShouldPass()
        {
            string url = controller.CalendarURL(1, "week", calendarID, apiKey);
            url = Regex.Replace(url, @"\+", "%2b");
            Assert.AreEqual(nextWeek, url);
        }


        /*******************************************************\
        * Tests for months
        \*******************************************************/
        // Devon test 5
        [Test()]
        public void CalendarURLTestMonthNowShouldPass()
        {
            string url = controller.CalendarURL(0, "month", calendarID, apiKey);
            url = Regex.Replace(url, @"\+", "%2b");
            Assert.AreEqual(thisMonth, url);
        }
        // Devon test 6
        [Test()]
        public void CalendarURLTestMonthLastShouldPass()
        {
            string url = controller.CalendarURL(-1, "month", calendarID, apiKey);
            url = Regex.Replace(url, @"\+", "%2b");
            Assert.AreEqual(lastMonth, url);
        }
        // Devon test 7
        [Test()]
        public void CalendarURLTestMonthNextShouldPass()
        {
            string url = controller.CalendarURL(1, "month", calendarID, apiKey);
            url = Regex.Replace(url, @"\+", "%2b");
            Assert.AreEqual(nextMonth, url);
        }


        /*******************************************************\
        * Tests for years
        \*******************************************************/
        // Devon test 8
        [Test()]
        public void CalendarURLTestYearNowShouldPass()
        {
            string url = controller.CalendarURL(0, "year", calendarID, apiKey);
            url = Regex.Replace(url, @"\+", "%2b");
            Assert.AreEqual(thisYear, url);
        }
        // Devon test 9
        [Test()]
        public void CalendarURLTestYearLastShouldPass()
        {
            string url = controller.CalendarURL(-1, "year", calendarID, apiKey);
            url = Regex.Replace(url, @"\+", "%2b");
            Assert.AreEqual(lastYear, url);
        }
        // Devon test 10
        [Test()]
        public void CalendarURLTestYearNextShouldPass()
        {
            string url = controller.CalendarURL(1, "year", calendarID, apiKey);
            url = Regex.Replace(url, @"\+", "%2b");
            Assert.AreEqual(nextYear, url);
        }

        /*******************************************************\
        * Fuzzing tests
        \*******************************************************/
        // Devon test 11
        [Test()]
        public void CalendarURLLowOutOfBoundsTestShouldPass()
        {
            string url = controller.CalendarURL(-20, null, calendarID, apiKey);
            Assert.AreEqual(defaultCalendar, url);
        }
        // Devon test 12
        [Test()]
        public void CalendarURLOutOfBoundsTestShouldPass()
        {
            string url = controller.CalendarURL(1000, null, calendarID, apiKey);
            Assert.AreEqual(defaultCalendar, url);
        }
        /// <summary>
        /// Checks for handling of all things null
        /// </summary>
        // Devon test 13
        [Test()]
        public void CalendarURLLowerNullTestShouldPass()
        {
            string url = controller.CalendarURL(-1, null, null, null);
            Assert.AreEqual(defaultNullCalendar, url);
        }
        // Devon test 14
        [Test()]
        public void CalendarURLUpperNullTestShouldPass()
        {
            string url = controller.CalendarURL(1, null, null, null);
            Assert.AreEqual(defaultNullCalendar, url);
        }
        /// <summary>
        /// Checks for a default int and default string states.
        /// </summary>
        // Devon test 15
        [Test()]
        public void CalendarURLNullTestShouldPass()
        {
            string url = controller.CalendarURL(0, null, null, null);
            Assert.AreEqual(defaultNullCalendar, url);
        }
    }
}