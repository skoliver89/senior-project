﻿using Moq;
using NUnit.Framework;
using Remi.Controllers;
using Remi.Models;
using Remi.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Remi.Controllers.Tests
{
    [TestFixture()]
    public class AuthenticationControllerTests
    {
        private Mock<IRepository> mock;

        [SetUp]
        public void SetupUserMock()
        {
            mock = new Mock<IRepository>();
            //mock.Setup(u => u.GetUserByID(1)).Returns(new User() { UserID = 1, UserName = "TestUser", Email = "test@test.com", CipheredPassword = "Xyum7g+Tt7u+ZigACded/Hdl8UzY7l5CwROQ7968jfJjQWSiWSfIn/TNRmkKkiLM7Xo8G+1S20WuBOFPfBwWXanuuYekV8AlWvuKG+xI4xIu8Vwkh4UjDk/KPU9VVLTd", PasswordSalt = "PasswordSalt" });
            //mock.Setup(u => u.GetUserByEmail("test@test.com")).Returns(new User() { UserID = 2, UserName = "TestUser", Email = "test@test.com", CipheredPassword = "Xyum7g+Tt7u+ZigACded/Hdl8UzY7l5CwROQ7968jfJjQWSiWSfIn/TNRmkKkiLM7Xo8G+1S20WuBOFPfBwWXanuuYekV8AlWvuKG+xI4xIu8Vwkh4UjDk/KPU9VVLTd", PasswordSalt = "PasswordSalt" });
            mock.Setup(p => p.GetProfileByID(1)).Returns(new Profile() { ProfileID = 1, Alias = "Test", CalendarID = "reviaitest@gmail.com", HomeLoc = "Monmouth, Oregon", UserID = 1 });
            mock.Setup(p => p.GetProfileByUserID(1)).Returns(new Profile() { ProfileID = 1, Alias = "Test", CalendarID = "reviaitest@gmail.com", HomeLoc = "Monmouth, Oregon", UserID = 1 });
        }

        // Feature 202
        // Jake's Test
        [Test()]
        public void CanEncryptAndDecryptSampleStringCorrectly()
        {
            // Arrange
            var password = "secret123";
            var salt = "SaLtY";

            // Act
            var cipherText = AuthenticationController.Encrypt(password, salt);
            var decryptedText = AuthenticationController.Decrypt(cipherText, salt);

            // Assert
            Assert.AreEqual(password, decryptedText);
        }

        // Feature 202
        // Jake's Test
        [Test()]
        public void DecryptCipherWithPhraseEqualsPlainText()
        {
            // Arrange
            var cipherText = "Xyum7g+Tt7u+ZigACded/Hdl8UzY7l5CwROQ7968jfJjQWSiWSfIn/TNRmkKkiLM7Xo8G+1S20WuBOFPfBwWXanuuYekV8AlWvuKG+xI4xIu8Vwkh4UjDk/KPU9VVLTd";

            var password = "Hello@2";
            var salt = "PasswordSalt";

            //Act
            var result = AuthenticationController.Decrypt(cipherText, salt);

            // Assert
            Assert.AreEqual(result, password);
        }

        // Feature 202
        // Jake's Test
        [Test()]
        public void DecryptMultipleCipherWithSameSaltAndPasswordEqualsSameDecipheredText()
        {
            // Arrange
            var cipherText1 = "Xyum7g+Tt7u+ZigACded/Hdl8UzY7l5CwROQ7968jfJjQWSiWSfIn/TNRmkKkiLM7Xo8G+1S20WuBOFPfBwWXanuuYekV8AlWvuKG+xI4xIu8Vwkh4UjDk/KPU9VVLTd";
            var cipherText2 = "3LF5N4lwJZRl/IGXNqOqqPtraj1qgBR4fkbmVCLYLe+Z4hafvxaOD1iZWhk0D17Z3eQ/lvt8EdELElv81xNUi1PRNwzRDWpTdX/wGh5BMrm6/jXYZmkuPXgNQt8qZrva";
            var password = "Hello@2";
            var salt = "PasswordSalt";

            //Act
            var result1 = AuthenticationController.Decrypt(cipherText1, salt);
            var result2 = AuthenticationController.Decrypt(cipherText2, salt);

            // Assert
            Assert.AreEqual(result1, password);
            Assert.AreEqual(result2, password);
            Assert.AreEqual(result1, result2);
        }

        // Feature 202
        // Jake's Test
        [Test()]
        public void EncryptingTheSamePlainTextWithTheSamePasswordMultipleTimesProducesDifferentCipherText()
        {
            // Arrange
            var password = "P@ssW3rd";
            var salt = "SaLtY";

            // Act
            var cipherText1 = AuthenticationController.Encrypt(password, salt);
            var cipherText2 = AuthenticationController.Encrypt(password, salt);

            // Assert
            Assert.AreNotEqual(cipherText1, cipherText2);
        }

        // Feature 202
        // Jake's Test
        [Test()]
        public void EncryptingTheSamePlainTextWithTheSamePasswordMultipleTimesProducesDifferentCipherTextButBothCanBeDecryptedCorrectly()
        {
            // Arrange
            var password = "somethingGood";
            var salt = "SaLtY";

            // Act
            var cipherText1 = AuthenticationController.Encrypt(password, salt);
            var cipherText2 = AuthenticationController.Encrypt(password, salt);
            var decryptedText1 = AuthenticationController.Decrypt(cipherText1, salt);
            var decryptedText2 = AuthenticationController.Decrypt(cipherText2, salt);

            // Assert
            Assert.AreEqual(decryptedText1, password);
            Assert.AreEqual(decryptedText1, decryptedText2);

        }
        
        // Abby tests for login
        [Test()]
        public void AddAuthUserInvalidLogin_GivesCorrectErrorMessage()
        {
            Dictionary<String, object> authUser = new Dictionary<String, object>();
            String expectedErrorMessage = "Invalid Email and/or Password";
            authUser = AuthenticationController.AddAuthUserInvalidLogin();
            Assert.AreEqual(expectedErrorMessage, authUser["ErrorMessage"]);
        }

        [Test()]
        public void AddAuthUserInvalidLogin_DoesNotAuthorizeUser()
        {
            Dictionary<String, object> authUser = new Dictionary<string, object>();
            authUser = AuthenticationController.AddAuthUserInvalidLogin();
            Assert.AreEqual(authUser["Authorized"], false);
        }

        [Test()] 
        public void AddAuthUserInvalidLogin_HasNoUID()
        {
            Dictionary<String, object> authUser = new Dictionary<string, object>();
            authUser = AuthenticationController.AddAuthUserInvalidLogin();
            Assert.IsNull(authUser["UID"]);
        }

        [Test()]
        public void AddAuthUserValidated_HasUID()
        {
            Dictionary<String, object> authUser = new Dictionary<string, object>();
            authUser = AuthenticationController.AddAuthUserValidated(1);
            Assert.IsNotNull(authUser["UID"]);
        }

        [Test()]
        public void AddAuthUserValidated_HasCorrectUID()
        {
            Dictionary<String, object> authUser = new Dictionary<string, object>();
            int expectedID = 123;
            int expectedID2 = 00012303;

            authUser = AuthenticationController.AddAuthUserValidated(expectedID);
            Assert.AreEqual(expectedID, authUser["UID"]);
            authUser = AuthenticationController.AddAuthUserValidated(expectedID2);
            Assert.AreEqual(expectedID2, authUser["UID"]);
        }

        [Test()] 
        public void AddAuthUserValidated_UserIsAuthorized()
        {
            Dictionary<String, object> authUser = new Dictionary<string, object>();
            authUser = AuthenticationController.AddAuthUserValidated(1);
            Assert.AreEqual(authUser["Authorized"], true);
        }

        [Test()]
        public void AddAuthUserValidated_ErrorMessageIsNull()
        {
            Dictionary<String, object> authUser = new Dictionary<string, object>();
            authUser = AuthenticationController.AddAuthUserValidated(1);
            Assert.IsNull(authUser["ErrorMessage"]);
        }

        // Jake's Test
        // Feature 202
        [Test()]
        public void TestGetProfileByIdWhereIdCorrectReturnsProfile()
        {
            //Arrange
            UserController controller = new UserController(mock.Object);
            int id = 1;
            //Act
            var p = mock.Object.GetProfileByID(id);
            //Assert
            Assert.AreEqual(p.Alias, "Test"); // That(u.Email, Equals("test@test.com"));
        }

        // Jake's Test
        // Feature 202
        [Test()]
        public void TestGetProfileByIdWhereIdIncorrectReturnsNull()
        {
            //Arrange
            UserController controller = new UserController(mock.Object);
            int id = 2;
            //Act
            Profile p = mock.Object.GetProfileByUserID(id);
            //Assert
            Assert.IsNull(p);
        }

        // Jake's Test
        // Feature 202
        [Test()]
        public void TestGetProfileByUserIdWhereIdCorrectReturnsProfile()
        {
            //Arrange
            UserController controller = new UserController(mock.Object);
            int id = 1;
            //Act
            var p = mock.Object.GetProfileByID(id);
            //Assert
            Assert.AreEqual(p.Alias, "Test"); // That(u.Email, Equals("test@test.com"));
        }

        // Jake's Test
        // Feature 202
        [Test()]
        public void TestGetProfileByUserIdWhereIdIncorrectReturnsNull()
        {
            //Arrange
            UserController controller = new UserController(mock.Object);
            int id = 2;
            //Act
            Profile p = mock.Object.GetProfileByUserID(id);
            //Assert
            Assert.IsNull(p);
        }
    }
}